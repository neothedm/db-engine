package dm.dbengine;

import dm.dbengine.connection.ConnectionFactory;
import dm.dbengine.connection.pool.ConnectionPool;
import dm.dbengine.connection.pool.ConnectionPoolFactory;
import dm.dbengine.dialects.DbDialect;
import dm.dbengine.models.impl.DbModel;
import dm.dbengine.models.impl.DefaultModelBuilder;
import dm.dbengine.models.mapper.Mapper;
import dm.dbengine.transactions.Transaction;
import dm.dbengine.util.DatabaseConfig;

import java.sql.Connection;
import java.sql.Statement;

public class DbEngine {

    private static DatabaseConfig config;
    private static ConnectionPool connectionPool;
    private static ConnectionFactory connectionFactory;
    private static ConnectionPoolFactory connectionPoolFactory;
    private static DefaultModelBuilder modelBuilder;
    private static DbDialect dialect;
    private static Mapper mapper;

    public static void init() {
        loadDbDriver();
    }

    public static DatabaseConfig getConfig() {
        return config;
    }

    public static void setConfig(DatabaseConfig config) {
        DbEngine.config = config;
    }

    public static ConnectionFactory getConnectionFactory() {
        return connectionFactory;
    }

    public static void setConnectionFactory(ConnectionFactory connectionFactory) {
        DbEngine.connectionFactory = connectionFactory;
    }

    public static void loadDbDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }


    public static ConnectionPoolFactory getConnectionPoolFactory() {
        return connectionPoolFactory;
    }

    public static void setConnectionPoolFactory(ConnectionPoolFactory connectionPoolFactory) {
        DbEngine.connectionPoolFactory = connectionPoolFactory;
    }

    public static ConnectionPool getConnectionPool() {
        if (connectionPool == null)
            connectionPool = connectionPoolFactory.create();
        return connectionPool;
    }

    public static void setModelBuilder(DefaultModelBuilder modelBuilder) {
        DbEngine.modelBuilder = modelBuilder;
    }

    public static DefaultModelBuilder getModelBuilder() {
        return modelBuilder;
    }

    public static DbDialect getDialect() {
        if (dialect != null) return dialect;
        try {
            dialect = config.getDialect().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
        return dialect;
    }

    public static Mapper getMapper() {
        return mapper;
    }

    public static void setMapper(Mapper mapper) {
        DbEngine.mapper = mapper;
    }
}
