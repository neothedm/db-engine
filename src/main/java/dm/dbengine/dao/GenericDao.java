package dm.dbengine.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDao<T, PK extends Serializable> {

    List<T> getAll();

    T get(final PK id);

    boolean exists(PK id);

    void save(final T object);

    void remove(T object);

    void replicate(T object);

    T refresh(T object);

    List<T> getBy(String column, Object value);

    List<T> getBy(String column, Object value, int limit);

    List<T> getLike(String column, Object value);

    T update(T object);

    T getByUnique(String column, Object value);
}