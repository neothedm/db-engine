package dm.dbengine.dao.impl;

import dm.dbengine.DbEngine;
import dm.dbengine.dao.GenericDao;
import dm.dbengine.dialects.DeleteStatement;
import dm.dbengine.dialects.InsertStatement;
import dm.dbengine.dialects.Statement;
import dm.dbengine.dialects.UpdateStatement;
import dm.dbengine.models.impl.Column;
import dm.dbengine.models.impl.DbModel;
import dm.dbengine.util.Executor;
import dm.dbengine.util.exceptions.NotFoundException;
import dm.dbengine.util.exceptions.SqlException;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

public class DefaultDao<T, PK extends Serializable> implements GenericDao<T, PK> {

    private Class<T> clazz;

    public DefaultDao(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public List<T> getAll() {
        DbModel model = DbEngine.getModelBuilder().getModel(clazz);
        try {
            return Executor.execute(DbEngine.getDialect().select().from(model.getTableName()), clazz);
        } catch (SQLException e) {
            throw new SqlException(e);
        }
    }

    @Override
    public T get(PK id) {
        DbModel model = DbEngine.getModelBuilder().getModel(clazz);
        List<Column> primaryKey = model.getPrimaryColumns();
        if (primaryKey.isEmpty()) {
            throw new UnsupportedOperationException(String.format("Entity %s doesn't have a primary key specified!", clazz.getSimpleName()));
        }
        try {
            String idValue = DbEngine.getDialect().escape(id);
            List<T> entities = Executor.execute(DbEngine.getDialect().select().from(model.getTableName()).where(primaryKey.get(0).getName(), "=", idValue), clazz);
            if (entities.isEmpty())
                throw new NotFoundException(String.format("No entity found in table %s with primary key %s", model.getTableName(), id));
            if (entities.size() > 1)
                throw new SqlException(String.format("Duplicate entities found for the same primary key '%s' in table '%s'!", id, model.getTableName()));
            return entities.get(0);
        } catch (SQLException e) {
            throw new SqlException(e);
        }
    }

    @Override
    public boolean exists(PK id) {
        return get(id) != null;
    }

    @Override
    public void save(T object) {
        List<InsertStatement> statements = DbEngine.getDialect().getQueryBuilder().buildInsert(object);
        try {
            Executor.execute((String)null);
        } catch (SQLException e) {
            throw new SqlException(e);
        }
    }

    @Override
    public void remove(T object) {
        List<DeleteStatement> statements = DbEngine.getDialect().getQueryBuilder().buildDelete(object);
        Executor.execute(statements);
    }

    @Override
    public void replicate(T object) {

    }

    @Override
    public T refresh(T object) {
        DbModel model = DbEngine.getModelBuilder().getModel(object.getClass());
        return get(model.getValue(object, model.getPrimaryColumns().get(0).getName()));
    }

    @Override
    public List<T> getBy(String column, Object value) {
        try {
            DbModel model = DbEngine.getModelBuilder().getModel(clazz);
            return Executor.execute(DbEngine.getDialect().select().from(model.getTableName()).where(column, " = ", DbEngine.getDialect().escape(value)), clazz);
        } catch (SQLException e) {
            throw new SqlException(e);
        }
    }

    @Override
    public List<T> getBy(String column, Object value, int limit) {
        try {
            DbModel model = DbEngine.getModelBuilder().getModel(clazz);
            String valueObj = DbEngine.getDialect().escape(value);
            return Executor.execute(DbEngine.getDialect().select().from(model.getTableName()).where(column, " = ", valueObj).limit(limit), clazz);
        } catch (SQLException e) {
            throw new SqlException(e);
        }
    }

    @Override
    public List<T> getLike(String column, Object value) {
        try {
            DbModel model = DbEngine.getModelBuilder().getModel(clazz);
            String valueObj = DbEngine.getDialect().escape(value);
            return Executor.execute(DbEngine.getDialect().select().from(model.getTableName()).where(column, " like ", valueObj), clazz);
        } catch (SQLException e) {
            throw new SqlException(e);
        }
    }

    @Override
    public T update(T object) {
        UpdateStatement statement = DbEngine.getDialect().getQueryBuilder().buildUpdate(object);
        try {
            Executor.execute(statement);
            return null;
        } catch (SQLException e) {
            throw new SqlException(e);
        }
    }

    @Override
    public T getByUnique(String column, Object value) {
        List<T> result = getBy(column, value);
        if (result.isEmpty())
            return null;
        return result.get(0);
    }
}
