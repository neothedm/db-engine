package dm.dbengine.util;

import dm.dbengine.DbEngine;
import dm.dbengine.dialects.DeleteStatement;
import dm.dbengine.transactions.Transaction;
import dm.dbengine.util.resultset.Processor;
import dm.dbengine.util.resultset.Reader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Executor {

    private final static Logger logger = LoggerFactory.getLogger(Executor.class);

    public static boolean execute(String sql) throws SQLException {
        return Transaction.execute(() -> {
            Statement statement = Transaction.getConnection().createStatement();
            return statement.execute(sql);
        });
    }

    public static <T> List<T> execute(String sql, Reader<T> reader) throws SQLException {
        return Transaction.execute(() -> {
            Statement statement = Transaction.getConnection().createStatement();
            statement.execute(sql);
            ResultSet resultSet = statement.getResultSet();
            List<T> result = new ArrayList<>();
            while (resultSet.next()) {
                T t = reader.read(resultSet);
                if (t != null)
                    result.add(t);
            }
            return result;
        });
    }

    public static <T> T execute(String sql, Processor<T> processor) throws SQLException {
        return Transaction.execute(() -> {
            Statement statement = Transaction.getConnection().createStatement();
            statement.execute(sql);
            ResultSet resultSet = statement.getResultSet();
            return processor.process(resultSet);
        });
    }

    public static boolean execute(dm.dbengine.dialects.Statement statement) throws SQLException {
        return execute(statement.toString());
    }

    public static <T> List<T> execute(dm.dbengine.dialects.Statement statement, Reader<T> reader) throws SQLException {
        return execute(statement.toString(), reader);
    }

    public static <T> T execute(dm.dbengine.dialects.Statement statement, Processor<T> processor) throws SQLException {
        return execute(statement.toString(), processor);
    }

    public static <T> List<T> execute(dm.dbengine.dialects.Statement statement, Class<T> clazz) throws SQLException {
        return execute(statement.toString(), clazz);
    }

    public static <T> List<T> execute(String sql, Class<T> clazz) throws SQLException {
        return execute(sql, (Reader<T>) resultSet -> {
            try {
                T entity = clazz.newInstance();
                return DbEngine.getMapper().map(entity, DbEngine.getModelBuilder().getModel(clazz), resultSet);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
            return null;
        });
    }

    public static void execute(List<? extends dm.dbengine.dialects.Statement> statements) {
        Transaction.execute(() -> {
            for (dm.dbengine.dialects.Statement statement : statements) {
                Executor.execute(statement);
            }
            return null;
        });
    }
}
