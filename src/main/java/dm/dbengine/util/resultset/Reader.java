package dm.dbengine.util.resultset;

import java.sql.ResultSet;

public interface Reader<T> {
    T read(ResultSet resultSet);
}