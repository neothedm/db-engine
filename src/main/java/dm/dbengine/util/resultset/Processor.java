package dm.dbengine.util.resultset;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface Processor<T> {
    T process(ResultSet resultSet) throws SQLException;
}
