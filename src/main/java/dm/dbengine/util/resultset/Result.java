package dm.dbengine.util.resultset;

import dm.dbengine.DbEngine;
import dm.dbengine.models.impl.DbModel;

import java.sql.ResultSet;

public class Result {

    private ResultSet resultSet;

    public ResultSet getResultSet() {
        return resultSet;
    }

    public <T> T to(Class<T> t) throws Exception {
        T instance = t.newInstance();
        DbModel model = DbEngine.getModelBuilder().getModel(t);
        return DbEngine.getMapper().map(instance, model, resultSet);
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }
}
