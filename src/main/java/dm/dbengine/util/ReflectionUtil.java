package dm.dbengine.util;

public class ReflectionUtil {

    final static Class[] numerics = new Class[]{ int.class, double.class, float.class,
            long.class, short.class, byte.class, Integer.class, Double.class,
            Float.class, Long.class, Short.class, Byte.class};

    public static boolean isNumeric(Object object) {
        return isNumeric(object.getClass());
    }

    public static boolean isNumeric(Class type) {
        for (Class numeric : numerics) {
            if (numeric.equals(type))
                return true;
        }
        return false;
    }
}
