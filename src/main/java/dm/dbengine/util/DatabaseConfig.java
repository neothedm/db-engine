package dm.dbengine.util;

import dm.dbengine.dialects.DbDialect;
import dm.dbengine.dialects.standard.StandardDialect;

public class DatabaseConfig {

    private String jdbcUrl;
    private String username;
    private String password;
    private int maxConnections;
    private Long connectionMaxLife;
    private Class<? extends DbDialect> dialect;

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getMaxConnections() {
        return maxConnections;
    }

    public void setMaxConnections(int maxConnections) {
        this.maxConnections = maxConnections;
    }

    public long getConnectionMaxLife() {
        return connectionMaxLife;
    }

    public void setConnectionMaxLife(long connectionMaxLife) {
        this.connectionMaxLife = connectionMaxLife;
    }

    public void setDialect(Class<? extends DbDialect> dialect) {
        this.dialect = dialect;

    }

    public Class<? extends DbDialect> getDialect() {
        return dialect;
    }
}
