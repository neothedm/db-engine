package dm.dbengine.util.exceptions;

public class SqlException extends RuntimeException {

    public SqlException(Throwable e) {
        super(e);
    }

    public SqlException(String msg) {
        super(msg);
    }
}
