package dm.dbengine.dialects;

public interface DeleteStatement extends Statement {

    DeleteStatement from(String tableName);

    DeleteStatement where(String... conditions);

    DeleteStatement where(String column, String operator, String value);

    DeleteStatement and(String... conditions);

    DeleteStatement and(String column, String operator, String value);

    DeleteStatement or(String... conditions);

    DeleteStatement or(String column, String operator, String value);
}
