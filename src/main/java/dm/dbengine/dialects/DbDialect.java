package dm.dbengine.dialects;

import dm.dbengine.dialects.standard.QueryBuilder;

public interface DbDialect {

    SelectStatement select();

    InsertStatement insert();

    UpdateStatement update();

    DeleteStatement delete();

    CreateTableStatement createTable();

    String escape(Object value);

    boolean canStoreDirectly(Class type);

    String getSqlType(Class type, int length);

    QueryBuilder getQueryBuilder();
}
