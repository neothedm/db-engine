package dm.dbengine.dialects;

public interface SelectStatement extends Statement {

    SelectStatement select(String... column);

    SelectStatement from(String... tableNames);

    SelectStatement where(String... conditions);

    SelectStatement where(String column, String operator, String value);

    SelectStatement and(String... conditions);

    SelectStatement and(String column, String operator, String value);

    SelectStatement or(String... conditions);

    SelectStatement or(String column, String operator, String value);

    SelectStatement orderByDesc(String... orderBys);

    SelectStatement orderByAsc(String... orderBys);

    Statement limit(int limit);

    Statement offset(int offset);
}
