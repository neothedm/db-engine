package dm.dbengine.dialects;

public interface UpdateStatement extends Statement {

    UpdateStatement update(String tableName);

    UpdateStatement set(String column, String value);

    UpdateStatement where(String... conditions);

    UpdateStatement where(String column, String operator, String value);

    UpdateStatement and(String... conditions);

    UpdateStatement and(String column, String operator, String value);

    UpdateStatement or(String... conditions);

    UpdateStatement or(String column, String operator, String value);
}
