package dm.dbengine.dialects;

public interface InsertStatement extends Statement {

    InsertStatement into(String tableName);

    InsertStatement columns(String... columns);

    InsertStatement values(String... values);
}
