package dm.dbengine.dialects.standard;

import dm.dbengine.dialects.UpdateStatement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StandardUpdateStatement implements UpdateStatement {

    private String tableName;
    private Map<String, String> values = new HashMap<>();
    private List<Condition> conditions = new ArrayList<>();

    @Override
    public UpdateStatement update(String tableName) {
        this.tableName = tableName;
        return this;
    }

    @Override
    public UpdateStatement set(String column, String value) {
        values.put(column, value);
        return this;
    }

    @Override
    public UpdateStatement where(String... conditions) {
        for (String condition : conditions) {
            this.conditions.add(new Condition("", condition));
        }
        return this;
    }

    @Override
    public UpdateStatement where(String column, String operator, String value) {
        this.conditions.add(new Condition("", column, operator, value));
        return this;
    }

    @Override
    public UpdateStatement and(String... conditions) {
        for (String condition : conditions) {
            this.conditions.add(new Condition("AND", condition));
        }
        return this;
    }

    @Override
    public UpdateStatement and(String column, String operator, String value) {
        this.conditions.add(new Condition("AND", column, operator, value));
        return this;
    }

    @Override
    public UpdateStatement or(String... conditions) {
        for (String condition : conditions) {
            this.conditions.add(new Condition("OR", condition));
        }
        return this;
    }

    @Override
    public UpdateStatement or(String column, String operator, String value) {
        this.conditions.add(new Condition("OR", column, operator, value));
        return this;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("UPDATE ").append(tableName);

        for (Map.Entry<String, String> entry : values.entrySet()) {
            builder.append(" ").append(entry.getKey());
            builder.append(" = ");
            builder.append(entry.getValue());
        }

        if (!conditions.isEmpty()) {
            builder.append(" WHERE ");
            for (int i = 0; i < conditions.size(); i++) {
                if (i == 0) {
                    builder.append(conditions.get(i).condition);
                } else {
                    builder.append(" ").append(conditions.get(i).joint).append(" ").append(conditions.get(i).condition);
                }
            }
        }
        return builder.toString();
    }
}
