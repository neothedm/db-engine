package dm.dbengine.dialects.standard;

import dm.dbengine.dialects.*;
import dm.dbengine.util.ReflectionUtil;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StandardDialect implements DbDialect {

    private final static String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private final static String NULL = "NULL";

    @Override
    public SelectStatement select() {
        return new StandardSelectStatement();
    }

    @Override
    public InsertStatement insert() {
        return new StandardInsertStatement();
    }

    @Override
    public UpdateStatement update() {
        return new StandardUpdateStatement();
    }

    @Override
    public DeleteStatement delete() {
        return new StandardDeleteStatement();
    }

    @Override
    public CreateTableStatement createTable() {
        return new StandardCreateTableStatement();
    }

    @Override
    public String escape(Object value) {
        if (value == null)
            return NULL;

        if (value instanceof Boolean)
            return (boolean) value? "1": "0";

        if (value instanceof Date)
            return quote(convert((Date)value));

        if (value instanceof Integer && (Integer)value == 0) {
            return NULL;
        }
        if (value instanceof Float && (Float)value == 0) {
            return NULL;
        }
        if (value instanceof Long && (Long)value == 0) {
            return NULL;
        }
        if (value instanceof Double && (Double)value == 0) {
            return NULL;
        }

        if (ReflectionUtil.isNumeric(value))
            return String.valueOf(value);

        return quote(String.valueOf(value));
    }

    private final static Class[] storables = new Class[]{Date.class, String.class};

    @Override
    public boolean canStoreDirectly(Class type) {
        if (ReflectionUtil.isNumeric(type))
            return true;

        for (Class storable : storables) {
            if (type.equals(storable))
                return true;
        }

        return false;
    }

    @Override
    public String getSqlType(Class type, int length) {
        String sqlType = null;
        if (type == Integer.TYPE || type == Integer.class)
            sqlType =  "INT";
        if (type == Long.TYPE || type == Long.class)
            sqlType =  "BIGINT";
        if (type == Date.class)
            sqlType =  "DATETIME";
        if (type == String.class)
            sqlType =  "NVARCHAR";
        if (type == Boolean.TYPE || type == Boolean.class)
            sqlType =  "BIT";
        if (type == Float.TYPE || type == Float.class)
            sqlType =  "FLOAT";
        if (type == Double.TYPE || type == Double.class)
            sqlType =  "DECIMAL";

        if (sqlType == null)
            throw new UnsupportedOperationException();

        if (length > 0) {
            sqlType += "(" + length + ")";
        } else {
            length = getDefaultLength(sqlType);
            if (length > 0)
                sqlType += "(" + getDefaultLength(sqlType) + ")";
        }

        return sqlType;
    }

    @Override
    public QueryBuilder getQueryBuilder() {
        return new QueryBuilder();
    }

    private int getDefaultLength(String sqlType) {
        return sqlType.equals("NVARCHAR")? 255: 0;
    }


    private String quote(String value) {
        return "'" + value + "'";
    }

    private String convert(Date date) {
        return new SimpleDateFormat(DATE_FORMAT).format(date);
    }
}
