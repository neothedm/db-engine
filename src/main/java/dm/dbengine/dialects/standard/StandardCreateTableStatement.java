package dm.dbengine.dialects.standard;

import dm.dbengine.dialects.CreateTableStatement;
import dm.dbengine.dialects.standard.constraints.Constraint;

import java.util.ArrayList;
import java.util.List;

public class StandardCreateTableStatement implements CreateTableStatement {

    private String table;
    private List<Column> columns = new ArrayList<>();
    private List<Constraint> constraints = new ArrayList<>();

    @Override
    public CreateTableStatement create(String tableName) {
        table = tableName;
        return this;
    }

    @Override
    public CreateTableStatement column(String name, String type) {
        columns.add(new Column(name, type));
        return this;
    }

    @Override
    public CreateTableStatement column(String name, String type, String properties) {
        columns.add(new Column(name, type, properties));
        return this;
    }

    @Override
    public CreateTableStatement constraint(Constraint constraint) {
        constraints.add(constraint);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("CREATE TABLE ").append(table).append("(");
        for (int i = 0; i < columns.size(); i++) {
            builder.append(columns.get(i));
            if (columns.size() -1 > i)
                builder.append(", ");
        }

        if (!constraints.isEmpty()) {
            builder.append(", ");
            for (int i = 0; i < constraints.size(); i++) {
                builder.append(constraints.get(i));
                if (constraints.size() - 1 > i)
                    builder.append(", ");
            }
        }

        return builder.append(")").toString();
    }
}
