package dm.dbengine.dialects.standard;

import dm.dbengine.dialects.DeleteStatement;

import java.util.ArrayList;
import java.util.List;

public class StandardDeleteStatement implements DeleteStatement {

    private String tableName;
    private List<Condition> conditions = new ArrayList<>();

    @Override
    public DeleteStatement from(String tableName) {
        this.tableName = tableName;
        return this;
    }

    @Override
    public DeleteStatement where(String... conditions) {
        for (String condition : conditions) {
            this.conditions.add(new Condition("", condition));
        }
        return this;
    }

    @Override
    public DeleteStatement where(String column, String operator, String value) {
        this.conditions.add(new Condition("AND", column, operator, value));
        return this;
    }

    @Override
    public DeleteStatement and(String... conditions) {
        for (String condition : conditions) {
            this.conditions.add(new Condition("AND", condition));
        }
        return this;
    }

    @Override
    public DeleteStatement and(String column, String operator, String value) {
        this.conditions.add(new Condition("AND", column, operator, value));
        return this;
    }

    @Override
    public DeleteStatement or(String... conditions) {
        for (String condition : conditions) {
            this.conditions.add(new Condition("OR", condition));
        }
        return this;
    }

    @Override
    public DeleteStatement or(String column, String operator, String value) {
        this.conditions.add(new Condition("OR", column, operator, value));
        return this;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("DELETE FROM ").append(tableName);

        if (!conditions.isEmpty()) {
            builder.append(" WHERE ");
            for (int i = 0; i < conditions.size(); i++) {
                if (i == 0) {
                    builder.append(conditions.get(i).condition);
                } else {
                    builder.append(" ").append(conditions.get(i).joint).append(" ").append(conditions.get(i).condition);
                }
            }
        }
        return builder.toString();
    }
}
