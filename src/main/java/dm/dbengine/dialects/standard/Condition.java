package dm.dbengine.dialects.standard;

public class Condition {

    String joint;
    String condition;

    public Condition(String condition) {
        this.condition = condition;
    }

    public Condition(String joint, String column, String operator, String value) {
        this.joint = joint;
        this.condition = column + " " + operator + " " + value;
    }

    public Condition(String joint, String condition) {
        this.joint = joint;
        this.condition = condition;
    }
}
