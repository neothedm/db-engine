package dm.dbengine.dialects.standard;

import dm.dbengine.DbEngine;
import dm.dbengine.dialects.*;
import dm.dbengine.dialects.standard.constraints.ForeignKey;
import dm.dbengine.dialects.standard.constraints.PrimaryKey;
import dm.dbengine.models.impl.Column;
import dm.dbengine.models.impl.DbModel;
import dm.dbengine.util.ReflectionUtil;

import java.util.*;

public class QueryBuilder {

    public List<InsertStatement> buildInsert(Object entity) {
        List<InsertStatement> statements = new ArrayList<>();
        DbModel model = DbEngine.getModelBuilder().getModel(entity.getClass());
        while (model != null) {
            InsertStatement insert = DbEngine.getDialect().insert();
            insert.into(model.getTableName());
            final DbModel finalModel = model;
            model.getColumns().forEach((name, column) -> {
                String value = DbEngine.getDialect().escape(column.getValue(entity));
                if (finalModel.getPrimaryColumns().contains(column) && finalModel.getParent() != null) {
                    insert.columns(column.getName());
                    insert.values("LAST_INSERT_ID()");
                } else if (!column.isGeneratedValue()) {
                    if (value != null) {
                        insert.columns(column.getName());
                        insert.values(value);
                    }
                }
            });
            statements.add(insert);
            model = model.getParent();
        }
        Collections.reverse(statements);
        return statements;
    }

    public SelectStatement buildSelect(Class type) {
        DbModel model = DbEngine.getModelBuilder().getModel(type);
        SelectStatement select = DbEngine.getDialect().select();
        while (model != null) {
            select.from(model.getTableName());
            final DbModel finalModel = model;
            model.getColumns().forEach((name, column) -> {
                select.select(finalModel.getTableName() + "." + name);
            });
            if (model.getParent() != null) {
                select.where(model.getTableName() + "." + model.getPrimaryColumns().get(0).getName(), " = ", model.getParent().getTableName() + "." + model.getParent().getPrimaryColumns().get(0).getName());
            }
            model = model.getParent();
        }
        return select;
    }

    public SelectStatement buildSelect(Object entity) {
        DbModel model = DbEngine.getModelBuilder().getModel(entity.getClass());
        SelectStatement select = DbEngine.getDialect().select();
        while (model != null) {
            select.from(model.getTableName());
            final DbModel finalModel = model;
            model.getColumns().forEach((name, column) -> {
                select.select(finalModel.getTableName() + "." + name);
                String value = DbEngine.getDialect().escape(column.getValue(entity));
                if (value != null) {
                    select.where(finalModel.getTableName() + "." + name, "=", value);
                }
            });
            if (model.getParent() != null) {
                select.where(model.getTableName() + "." + model.getPrimaryColumns().get(0).getName(), " = ", model.getParent().getTableName() + "." + model.getParent().getPrimaryColumns().get(0).getName());
            }
            model = model.getParent();
        }
        return select;
    }

    public UpdateStatement buildUpdate(Object entity) {
        DbModel model = DbEngine.getModelBuilder().getModel(entity.getClass());
        UpdateStatement update = DbEngine.getDialect().update();
        update.update(model.getTableName());

        if (model.getPrimaryColumns().isEmpty())
            throw new IllegalStateException();

        model.getPrimaryColumns().forEach((column) -> {

            Object primaryKey = column.getValue(entity);

            if (primaryKey == null)
                throw new IllegalStateException();

            update.where(column.getName(), DbEngine.getDialect().escape(primaryKey));
        });

        model.getColumns().forEach((name, column) -> {

            if (column.isId())
                return;

            String value = DbEngine.getDialect().escape(column.getValue(entity));
            if (value != null) {
                update.set(name, value);
            }
        });
        return update;
    }

    public List<DeleteStatement> buildDelete(Object entity) {
        DbModel model = DbEngine.getModelBuilder().getModel(entity.getClass());
        List<DeleteStatement> statements = new ArrayList<>();
        while (model != null) {
            DeleteStatement delete = DbEngine.getDialect().delete();
            delete.from(model.getTableName());

            final boolean[] primaryKeyUsed = {false};

            model.getPrimaryColumns().forEach((column) -> {
                Object primaryKey = column.getValue(entity);
                if (primaryKey != null && !(ReflectionUtil.isNumeric(primaryKey) && !primaryKey.equals(0))) {
                    delete.where(column.getName(), "=", DbEngine.getDialect().escape(primaryKey));
                    primaryKeyUsed[0] = true;
                }
            });

            if (!primaryKeyUsed[0]) {
                model.getColumns().forEach((name, column) -> {
                    if (column.isId()) return;

                    String value = DbEngine.getDialect().escape(column.getValue(entity));
                    if (value != null) {
                        delete.where(name, "=", value);
                    }
                });
            }
            statements.add(delete);
            model = model.getParent();
        }
        return statements;
    }

    public List<CreateTableStatement> buildCreateTable(Class clazz) {
        Map<DbModel, CreateTableStatement> statements = new HashMap<>();
        DbModel model = DbEngine.getModelBuilder().getModel(clazz);
        while (model != null) {
            CreateTableStatement createTable = DbEngine.getDialect().createTable();
            createTable.create(model.getTableName());

            model.getColumns().forEach((name, column) -> {
                createColumnDef(createTable, column);
            });

            final DbModel mdl = model;
            model.getColumns().forEach((name, column) -> {
                createColumnConstraint(createTable, mdl, column);
            });

            statements.put(model, createTable);
            model = model.getParent();
        }

        statements.forEach((dbModel, statement) -> {
            if (dbModel.sharesTableWithParent()) {
                Column column = dbModel.getPrimaryColumns().get(0);
                Column parentPrimary = dbModel.getParent().getPrimaryColumns().get(0);
                statement.constraint(new ForeignKey(column.getName(), dbModel.getParent().getTableName(), parentPrimary.getName()));
            }
        });

        List<CreateTableStatement> result = new ArrayList<>();
        statements.forEach((m, s) -> result.add(s));

        Collections.reverse(result);
        return result;
    }

    private void createColumnConstraint(CreateTableStatement statement, DbModel dbModel, Column column) {
        if (column.isId() || dbModel.getPrimaryColumns().contains(column))
            statement.constraint(new PrimaryKey(column.getName()));

        if (column.isForeignEntity()) {
            DbModel foreignModel = DbEngine.getModelBuilder().getModel(column.getForeignEntity());
            statement.constraint(new ForeignKey(column.getName(), foreignModel.getTableName(), foreignModel.getPrimaryColumns().get(0).getName()));
        }
    }

    private void createColumnDef(CreateTableStatement statement, Column column) {
        StringBuilder properties = new StringBuilder();
        if (column.isGeneratedValue())
            properties.append(" AUTO_INCREMENT");

        if (column.isUnique())
            properties.append(" UNIQUE");

        if (DbEngine.getDialect().canStoreDirectly(column.getType())) {
            statement.column(column.getName(), DbEngine.getDialect().getSqlType(column.getType(), column.getLength()), properties.toString());
        } else {
            // Only of one to one or many to one
            DbModel dbModel = DbEngine.getModelBuilder().getModel(column.getType());
            if (dbModel != null) {
                statement.column(column.getName(), DbEngine.getDialect().getSqlType(dbModel.getPrimaryColumns().get(0).getType(), column.getLength()), properties.toString());
            }
        }
    }
}
