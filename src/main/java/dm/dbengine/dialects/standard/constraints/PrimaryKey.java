package dm.dbengine.dialects.standard.constraints;

public class PrimaryKey extends Constraint {

    private String[] columns;

    public PrimaryKey(String... columns) {
        this.columns = columns;
    }

    @Override
    public String toString() {
        return "PRIMARY KEY (" + String.join(",", columns) + ")";
    }
}
