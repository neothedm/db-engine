package dm.dbengine.dialects.standard.constraints;

public class Constraint {

    private String name;
    private Type type;
    private String[] columns;

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public String[] getColumns() {
        return columns;
    }

    public enum Type {
        PRIMARY_KEY,
        FORIEGN_KEY,
        NOT_NULL,
        UNIQUE,
        DEFAULT,
        CHECK,
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CONSTRAINT ").append(name);
        switch (type) {
            case PRIMARY_KEY:
                builder.append(" PRIMARY KEY (").append(String.join(",", columns));
                break;
            case FORIEGN_KEY:
                builder.append(" FOREIGN KEY (").append(String.join(",", columns));
                break;
            case NOT_NULL:
                builder.append(" NOT NULL (").append(String.join(",", columns));
                break;
            case UNIQUE:
                builder.append(" UNIQUE (").append(String.join(",", columns));
                break;
            case DEFAULT:
                builder.append(" DEFAULT (").append(String.join(",", columns));
                break;
            case CHECK:
                builder.append(" CHECK (").append(String.join(",", columns));
                break;
            default:
                throw new IllegalStateException("NOT SUPPORTED CONSTRAINT");
        }

        builder.append(")");

        return builder.toString();
    }
}
