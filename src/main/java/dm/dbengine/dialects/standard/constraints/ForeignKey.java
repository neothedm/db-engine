package dm.dbengine.dialects.standard.constraints;

public class ForeignKey extends Constraint {

    private String column;
    private String foriegnTable;
    private String foriegnKey;

    public ForeignKey(String column, String foriegnTable, String foriegnKey) {
        this.column = column;
        this.foriegnTable = foriegnTable;
        this.foriegnKey = foriegnKey;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getForiegnTable() {
        return foriegnTable;
    }

    public void setForiegnTable(String foriegnTable) {
        this.foriegnTable = foriegnTable;
    }

    public String getForiegnKey() {
        return foriegnKey;
    }

    public void setForiegnKey(String foriegnKey) {
        this.foriegnKey = foriegnKey;
    }

    @Override
    public String toString() {
        return "FOREIGN KEY (" + column + ") REFERENCES " + foriegnTable + "(" + foriegnKey + ")";
    }
}
