package dm.dbengine.dialects.standard.constraints;

public class NotNull extends Constraint {

    private String column;

    public NotNull(String column) {
        this.column = column;
    }

    @Override
    public String toString() {
        return column + " NOT NULL";
    }
}
