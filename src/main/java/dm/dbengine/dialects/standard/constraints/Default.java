package dm.dbengine.dialects.standard.constraints;

public class Default {

    private String column;
    private String defaultValue;

    public Default(String column, String defaultValue) {
        this.column = column;
        this.defaultValue = defaultValue;
    }

    @Override
    public String toString() {
        return column + " DEFAULT " + defaultValue;
    }
}
