package dm.dbengine.dialects.standard.constraints;

public class Unique {

    private String column;

    public Unique(String column) {
        this.column = column;
    }

    @Override
    public String toString() {
        return "UNIQUE " + column;
    }
}
