package dm.dbengine.dialects.standard;

import dm.dbengine.dialects.SelectStatement;
import dm.dbengine.dialects.Statement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StandardSelectStatement implements SelectStatement {

    private List<String> tables = new ArrayList<>();
    private List<String> columns = new ArrayList<>();
    private List<Condition> conditions = new ArrayList<>();
    private List<OrderBy> orderBy = new ArrayList<>();
    private int limit;
    private int offset;

    @Override
    public SelectStatement select(String... column) {
        Collections.addAll(columns, column);
        return this;
    }

    @Override
    public SelectStatement from(String... tableNames) {
        Collections.addAll(this.tables, tableNames);
        return this;
    }

    @Override
    public SelectStatement where(String... conditions) {
        for (String condition : conditions) {
            this.conditions.add(new Condition("", condition));
        }
        return this;
    }

    @Override
    public SelectStatement where(String column, String operator, String value) {
        this.conditions.add(new Condition("", column, operator, value));
        return this;
    }

    @Override
    public SelectStatement and(String... conditions) {
        for (String condition : conditions) {
            this.conditions.add(new Condition("AND", condition));
        }
        return this;
    }

    @Override
    public SelectStatement and(String column, String operator, String value) {
        this.conditions.add(new Condition("AND", column, operator, value));
        return this;
    }

    @Override
    public SelectStatement or(String... conditions) {
        for (String condition : conditions) {
            this.conditions.add(new Condition("OR", condition));
        }
        return this;
    }

    @Override
    public SelectStatement or(String column, String operator, String value) {
        this.conditions.add(new Condition("OR", column, operator, value));
        return this;
    }

    @Override
    public SelectStatement orderByDesc(String... orderBys) {
        for (String orderBy : orderBys) {
            this.orderBy.add(new OrderBy(orderBy, "DESC"));
        }
        return this;
    }

    @Override
    public SelectStatement orderByAsc(String... orderBys) {
        for (String orderBy : orderBys) {
            this.orderBy.add(new OrderBy(orderBy, "ASC"));
        }
        return this;
    }

    @Override
    public Statement limit(int limit) {
        this.limit = limit;
        return this;
    }

    @Override
    public Statement offset(int offset) {
        this.offset = offset;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("SELECT ");

        if (columns.isEmpty()) {
            builder.append("* ");
        } else {
            for (int i = 0; i < columns.size(); i++) {
                builder.append(columns.get(i));
                if (columns.size() - 1 != i)
                    builder.append(", ");
            }
        }

        builder.append(" FROM ");
        for (int i = 0; i < tables.size(); i++) {
            builder.append(tables.get(i));
            if (tables.size() - 1 != i)
                builder.append(", ");
        }

        if (!conditions.isEmpty()) {
            builder.append(" WHERE ");
            for (int i = 0; i < conditions.size(); i++) {
                if (i == 0) {
                    builder.append(conditions.get(i).condition);
                } else {
                    builder.append(" ").append(conditions.get(i).joint).append(" ").append(conditions.get(i).condition);
                }
            }
        }

        if (!orderBy.isEmpty()) {
            builder.append(" ORDER BY ");
            for (int i = 0; i < orderBy.size(); i++) {
                builder.append(orderBy.get(i).column).append(" ").append(orderBy.get(i).direction);
                if (conditions.size() - 1 != i)
                    builder.append(", ");
            }
        }

        if (limit > 0) {
            builder.append(" LIMIT ").append(limit).append(" OFFSET ").append(offset);
        }

        return builder.toString();
    }

    public class OrderBy {
        String column;
        String direction;

        public OrderBy(String column) {
            this.column = column;
        }

        public OrderBy(String column, String direction) {
            this.column = column;
            this.direction = direction;
        }
    }
}
