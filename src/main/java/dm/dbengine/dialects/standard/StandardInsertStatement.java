package dm.dbengine.dialects.standard;

import dm.dbengine.dialects.InsertStatement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StandardInsertStatement implements InsertStatement {

    private String table;
    private List<String> columns = new ArrayList<>();
    private List<String> values = new ArrayList<>();

    @Override
    public InsertStatement into(String tableName) {
        table = tableName;
        return this;
    }

    @Override
    public InsertStatement columns(String... columns) {
        Collections.addAll(this.columns, columns);
        return null;
    }

    @Override
    public InsertStatement values(String... values) {
        Collections.addAll(this.values, values);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("INSERT INTO ").append(table);

        if (columns.size() != values.size())
            throw new IllegalStateException("Required columns are not supplied!");

        if (!columns.isEmpty()) {
            builder.append("(");
            for (int i = 0; i < columns.size(); i++) {
                builder.append(columns.get(i));
                if (columns.size() - 1 != i)
                    builder.append(", ");
            }
            builder.append(")");
        }

        builder.append(" VALUES ");
        builder.append("(");
        for (int i = 0; i < values.size(); i++) {
            builder.append(values.get(i));
            if (values.size() - 1 != i)
                builder.append(", ");
        }
        builder.append(")");

        return builder.toString();
    }
}
