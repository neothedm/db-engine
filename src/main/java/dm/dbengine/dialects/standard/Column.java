package dm.dbengine.dialects.standard;

public class Column {

    private String name;
    private String type;
    private String properties;

    public Column(String name, String type, String properties) {
        this.name = name;
        this.type = type;
        this.properties = properties;
    }

    public Column(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getProperties() {
        return properties;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(name).append(" ").append(type);

        if (properties != null) {
            builder.append(" ").append(String.join(" ", properties));
        }

        return builder.toString();
    }
}
