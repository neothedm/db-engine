package dm.dbengine.dialects;

import dm.dbengine.dialects.standard.constraints.Constraint;

public interface CreateTableStatement extends Statement {

    CreateTableStatement create(String tableName);

    CreateTableStatement column(String name, String type, String isNull);

    CreateTableStatement column(String name, String type);

    CreateTableStatement constraint(Constraint constraint);
}
