package dm.dbengine.connection.pool;

import dm.dbengine.connection.pool.ConnectionPool;

public interface ConnectionPoolFactory {
    ConnectionPool create();
}
