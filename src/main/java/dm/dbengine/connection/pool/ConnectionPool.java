package dm.dbengine.connection.pool;

import java.sql.Connection;
import java.sql.SQLException;

public interface ConnectionPool {
    Connection getConnection() throws SQLException;

    int getAvailableConnectionsCount();

    int getUsedConnectionsCount();

    int getTotalConnectionsCount();

    void release(Connection connection);
}
