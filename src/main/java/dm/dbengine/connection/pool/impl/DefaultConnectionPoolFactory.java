package dm.dbengine.connection.pool.impl;

import dm.dbengine.connection.pool.ConnectionPool;
import dm.dbengine.connection.pool.ConnectionPoolFactory;

public class DefaultConnectionPoolFactory implements ConnectionPoolFactory {

    @Override
    public ConnectionPool create() {
        return new DefaultConnectionPool();
    }
}
