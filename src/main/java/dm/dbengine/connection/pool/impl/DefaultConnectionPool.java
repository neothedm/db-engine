package dm.dbengine.connection.pool.impl;

import dm.dbengine.DbEngine;
import dm.dbengine.connection.pool.ConnectionPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class DefaultConnectionPool implements ConnectionPool, DataSource {

    Logger logger = LoggerFactory.getLogger(DefaultConnectionPool.class);

    private final Object lock = new Object();
    private Map<Connection, Long> availableConnections = new ConcurrentHashMap<>();
    private Set<Connection> usedConnections = new HashSet<>();
    private int loginTimeout;

    public DefaultConnectionPool() {
        terminator.start();
    }

    @Override
    public Connection getConnection() throws SQLException {
        synchronized (lock) {
            Connection connection = null; // will be returned if no connection is available or the max is reached, should be blocking in the future
            if (!availableConnections.isEmpty()) {
                connection = availableConnections.keySet().iterator().next();
                availableConnections.remove(connection);
                usedConnections.add(connection);
            }
            if (getTotalConnectionsCount() < DbEngine.getConfig().getMaxConnections()) {
                connection = createConnection();
                usedConnections.add(connection);
            }
            if (connection != null) {
                connection.setAutoCommit(false);

            }
            return connection;
        }
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return DbEngine.getConnectionFactory().create(username, password);
    }

    private Connection createConnection() {
        return DbEngine.getConnectionFactory().create();
    }

    @Override
    public int getAvailableConnectionsCount() {
        synchronized (lock)  {
            return availableConnections.size();
        }
    }

    @Override
    public int getUsedConnectionsCount() {
        synchronized (lock)  {
            return usedConnections.size();
        }
    }

    @Override
    public int getTotalConnectionsCount() {
        return usedConnections.size() + availableConnections.size();
    }

    @Override
    public void release(Connection connection) {
        synchronized (lock) {

            if (connection == null)
                throw new NullPointerException();

            try {
                if (!connection.isClosed()) {
                    usedConnections.remove(connection);
                    availableConnections.put(connection, System.currentTimeMillis());
                }
            } catch (SQLException e) {
                logger.error(e.getMessage(), e);
            }
        }
    }

    private Thread terminator = new Thread() {
        @Override
        public void run() {
            while (true) {
                try {
                    availableConnections.entrySet().stream().filter(entry -> entry.getValue() > DbEngine.getConfig().getConnectionMaxLife()).forEach(entry -> {
                        synchronized (lock) {
                            if (availableConnections.containsKey(entry.getKey())) {
                                availableConnections.remove(entry.getKey());
                            }
                        }
                    });
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    logger.info("ConnectionPool connection terminator quiting...");
                    return;
                }
            }
        }
    };

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return null;
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {

    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        this.loginTimeout = seconds;
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return loginTimeout;
    }

    @Override
    public java.util.logging.Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return null;
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }
}
