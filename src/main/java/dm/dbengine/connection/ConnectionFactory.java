package dm.dbengine.connection;

import java.sql.Connection;

public interface ConnectionFactory {
    Connection create();

    Connection create(String username, String password);
}
