package dm.dbengine.connection.impl;

import dm.dbengine.DbEngine;
import dm.dbengine.connection.ConnectionFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DefaultConnectionFactory implements ConnectionFactory {

    @Override
    public Connection create() {
        return create(DbEngine.getConfig().getUsername(), DbEngine.getConfig().getPassword());
    }

    @Override
    public Connection create(String username, String password) {
        try {
            return DriverManager.getConnection(DbEngine.getConfig().getJdbcUrl(),
                    DbEngine.getConfig().getUsername(), DbEngine.getConfig().getPassword());
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
        }
        return null;
    }
}
