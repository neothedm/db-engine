package dm.dbengine.models;

import dm.dbengine.models.impl.DbModel;

public interface ModelBuilder {

    DbModel buildModel(Class clazz);

    DbModel getModel(Class clazz);

    DbModel getByTableName(String tableName);
}
