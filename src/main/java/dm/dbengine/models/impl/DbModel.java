package dm.dbengine.models.impl;

import dm.dbengine.DbEngine;

import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DbModel {

    private String tableName;
    private List<Column> primaryColumns;
    private Map<String, Column> columns;
    private Class type;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<Column> getPrimaryColumns() {
        return primaryColumns;
    }

    public void setPrimaryColumns(List<Column> primaryColumns) {
        this.primaryColumns = primaryColumns;
    }

    public Map<String, Column> getColumns() {
        return columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns.stream().collect(Collectors.toMap(Column::getName, column -> column));
    }

    public Collection<String> getColumnNames() {
        if (columns == null)
            return Collections.emptySet();
        return columns.keySet();
    }

    public Collection<Column> getColumnList() {
        if (columns == null)
            return Collections.emptySet();
        return columns.values();
    }

    public DbModel getParent() {
        return DbEngine.getModelBuilder().getModel(type.getSuperclass());
    }

    public Class getType() {
        return type;
    }

    public void setType(Class type) {
        this.type = type;
    }

    public boolean sharesTableWithParent() {
        if (type.getSuperclass() == Object.class) return false;
        Inheritance inheritance = (Inheritance) type.getSuperclass().getDeclaredAnnotation(Inheritance.class);
        return (inheritance.strategy() != null && inheritance.strategy() == InheritanceType.JOINED);
    }

    @SuppressWarnings("unchecked")
    public <VALUE> VALUE getValue(Object object, String columnName) {
        try {
            Column column = getColumns().get(columnName);

            Field field = column.getField();

            if (field == null)
                return null;

            field.setAccessible(true);

            return (VALUE) field.get(object);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException();
        }
    }
}
