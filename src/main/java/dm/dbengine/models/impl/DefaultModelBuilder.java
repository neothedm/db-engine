package dm.dbengine.models.impl;

import dm.dbengine.DbEngine;
import dm.dbengine.models.ModelBuilder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import java.beans.Transient;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class DefaultModelBuilder implements ModelBuilder {

    private Map<Class, DbModel> modelCache = new ConcurrentHashMap<>();
    private Map<String, DbModel> tableCache = new ConcurrentHashMap<>();

    @Override
    public DbModel buildModel(Class clazz) {
        List<Column> columns = getColumns(clazz);

        DbModel model = new DbModel();
        model.setTableName(getTableName(clazz));
        model.setPrimaryColumns(getPrimaryColumns(columns));
        model.setColumns(columns);
        model.setType(clazz);

        return model;
    }

    @Override
    public DbModel getModel(Class clazz) {
        if (clazz == Object.class)
            return null;

        DbModel model = modelCache.get(clazz);

        if (model != null)
            return model;

        model = buildModel(clazz);

        modelCache.put(clazz, model);
        tableCache.put(model.getTableName(), model);

        return model;
    }

    /**
     * Note that this will only returned cached models, getModel(Class) needs to be called before this can return
     * a valid value
     */
    @Override
    public DbModel getByTableName(String tableName) {
        return tableCache.get(tableName);
    }

    private List<Column> getColumns(Class clazz) {
        List<Column> result = new ArrayList<>();
        for (Field field: collectFields(clazz)) {

            field.setAccessible(true);

            if (DbEngine.getDialect().canStoreDirectly(field.getType()) || isEntity(field.getType())) {
                Column column = new Column();
                column.setField(field);
                result.add(column);
            }
        }
        return result;
    }

    private boolean isEntity(Class<?> type) {
        Entity entity = type.getDeclaredAnnotation(Entity.class);
        return entity != null;
    }

    private List<Column> getPrimaryColumns(List<Column> columns) {
        return columns.stream().filter(Column::isId).collect(Collectors.toList());
    }

    private List<Field> collectFields(Class clazz) {
        Set<String> fieldNames = new HashSet<>();
        List<Field> result = new ArrayList<>();
        Class target = clazz;

        while (target != Object.class) {
            for (Field field : target.getDeclaredFields()) {
                if (!fieldNames.contains(field.getName()) && !field.isSynthetic() && !Modifier.isTransient(field.getModifiers()) &&
                        !field.isAnnotationPresent(Transient.class) && !Modifier.isStatic(field.getModifiers()) &&
                        !Modifier.isFinal(field.getModifiers())) {
                    result.add(field);
                    fieldNames.add(field.getName());
                }
            }

            target = target.getSuperclass();
            if (hasOwnTable(target))
                break;
        }
        return result;
    }

    private boolean hasOwnTable(Class target) {
        Inheritance inheritance = (Inheritance) target.getDeclaredAnnotation(Inheritance.class);
        return inheritance != null && inheritance.strategy() == InheritanceType.JOINED;
    }

    private String getTableName(Class clazz) {
        Entity table = (Entity) clazz.getDeclaredAnnotation(Entity.class);
        if (table != null && table.name() != null && !table.name().equals("")) {
            return table.name();
        }
        return inferTableName(clazz);
    }

    private String inferTableName(Class clazz) {
        String tableName = clazz.getSimpleName();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < tableName.length(); ++i) {
            if (Character.isUpperCase(tableName.charAt(i)) && i != 0)
                result.append("_");
            result.append(tableName.charAt(i));
        }
        return result.toString().toUpperCase();
    }
}
