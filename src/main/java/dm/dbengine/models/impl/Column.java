package dm.dbengine.models.impl;

import javax.persistence.*;
import java.lang.reflect.Field;
import java.util.List;

public class Column {

    private Field field;

    public String getName() {
        javax.persistence.Column column = field.getDeclaredAnnotation(javax.persistence.Column.class);
        if (column != null) {
            String name = column.name();
            if (name != null && !name.isEmpty())
                return name;
        }
        return field.getName();
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public boolean isUnique() {
        javax.persistence.Column column = field.getDeclaredAnnotation(javax.persistence.Column.class);
        return column != null && column.unique();
    }

    public int getLength() {
        javax.persistence.Column column = field.getDeclaredAnnotation(javax.persistence.Column.class);
        if (column == null)
            return 0;
        return column.length();
    }

    public boolean isGeneratedValue() {
        return field.isAnnotationPresent(GeneratedValue.class);
    }

    public boolean isId() {
        Id id = field.getDeclaredAnnotation(Id.class);
        return id != null || "id".equals(getName());
    }

    public boolean isForeignEntity() {
        return getForeignEntity() != null;
    }

    public Class getForeignEntity() {
        OneToOne oneToOne = field.getDeclaredAnnotation(OneToOne.class);

        Class type = null;

        if (oneToOne != null)
            type = oneToOne.targetEntity();

        OneToMany oneToMany = field.getDeclaredAnnotation(OneToMany.class);
        if (oneToMany != null)
            type = oneToMany.targetEntity();

        ManyToOne manyToOne = field.getDeclaredAnnotation(ManyToOne.class);
        if (manyToOne != null)
            type = manyToOne.targetEntity();

        ManyToMany manyToMany = field.getDeclaredAnnotation(ManyToMany.class);
        if (manyToMany != null)
            type = manyToMany.targetEntity();

        return type == void.class? field.getType(): type;
    }

    public Class getType() {
        return field.getType();
    }

    @SuppressWarnings("unchecked")
    public <VALUE> VALUE getValue(Object object) {
        try {

            if (field == null)
                return null;

            field.setAccessible(true);

            return (VALUE) field.get(object);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException();
        }
    }

    public void setValue(Object t, Object value) {
        try {
            if (field == null)
                return;

            field.setAccessible(true);

            field.set(t, value);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException();
        }
    }
}
