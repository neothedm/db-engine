package dm.dbengine.models.mapper;

import dm.dbengine.models.impl.DbModel;

import java.sql.ResultSet;

public interface Mapper {

    <T> T map(T t, DbModel model, ResultSet resultSet) throws Exception;
}
