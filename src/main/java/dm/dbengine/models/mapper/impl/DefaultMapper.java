package dm.dbengine.models.mapper.impl;

import dm.dbengine.models.impl.DbModel;
import dm.dbengine.models.mapper.Mapper;
import dm.dbengine.util.ReflectionUtil;
import dm.dbengine.util.exceptions.SqlException;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

public class DefaultMapper implements Mapper {

    @Override
    @SuppressWarnings("unchecked")
    public <T> T map(T t, DbModel model, ResultSet resultSet) throws Exception {
        while (model != null) {
            final DbModel finalModel = model;
            model.getColumns().forEach((name, column) -> {
                try {
                    Object value = resultSet.getObject(name);
                    if (value == null)
                        value = resultSet.getObject(finalModel.getTableName() + "." + name);
                    if (ReflectionUtil.isNumeric(column.getType()) && value == null) {
                        column.setValue(t, 0);
                    } else if (column.getType().isAssignableFrom(Boolean.TYPE) && value == null) {
                        column.setValue(t, false);
                    } else {
                        column.setValue(t, value);
                    }
                } catch (SQLException e) {
                    throw new SqlException(e);
                }
            });
            model = model.getParent();
        }
        return t;
    }
}
