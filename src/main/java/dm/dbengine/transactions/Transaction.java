package dm.dbengine.transactions;

import dm.dbengine.DbEngine;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.Callable;

public class Transaction {

    private static InheritableThreadLocal<Connection> connectionTL = new InheritableThreadLocal<>();

    public static <T> T execute(Callable<T> callable) {
        if (exists()) {
            return executeWithoutTransaction(callable);
        } else {
            return executeWithTransaction(callable);
        }
    }

    private static <T> T executeWithoutTransaction(Callable<T> callable) {
        try {
            return callable.call();
        } catch (Exception e) {
            throw new TransactionException(e);
        }
    }

    private static <T> T executeWithTransaction(Callable<T> callable) {
        Connection connection = connectionTL.get();
        boolean connectionsObtainedHere = false;
        try {
            if (connection == null) {
                connection = DbEngine.getConnectionPool().getConnection();
                connectionsObtainedHere = true;
                connectionTL.set(connection);
            }
            T t = callable.call();
            connection.commit();
            return t;
        } catch (Exception e) {
            try {
                if (connection != null)
                    connection.rollback();
            } catch (SQLException e1) {
                throw new TransactionException(e1);
            }
            throw new TransactionException(e);
        } finally {
            if (connection != null && connectionsObtainedHere) {
                DbEngine.getConnectionPool().release(connection);
                connectionTL.set(null);
            }
        }
    }

    public static Connection getConnection() {
        return connectionTL.get();
    }

    public static boolean exists() {
        return getConnection() != null;
    }
}
