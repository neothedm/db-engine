package dm.dbengine.transactions;

public class TransactionException extends RuntimeException {

    public TransactionException(Throwable t) {
        super(t);
    }
}
