package dm.dbengine.dummy;

import javax.persistence.*;
import java.util.Date;
@Inheritance(strategy = InheritanceType.JOINED)
public class TestObject {

    @Id
    @GeneratedValue
    private long id;
    private String name;
    @Column(unique = true)
    private String code;
    private Date timeObject;
    private boolean state;
//    @OneToOne(targetEntity = TestObject2.class)
//    private TestObject2 foreignn;
//    private TestObject2 testObject2;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getTimeObject() {
        return timeObject;
    }

    public void setTimeObject(Date timeObject) {
        this.timeObject = timeObject;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

//    public TestObject2 getTestObject2() {
//        return testObject2;
//    }

//    public void setTestObject2(TestObject2 testObject2) {
//        this.testObject2 = testObject2;
//    }

//    public TestObject2 getForeign() {
//        return foreignn;
//    }

//    public void setForeignId(TestObject2 foreign) {
//        this.foreignn = foreign;
//    }
}
