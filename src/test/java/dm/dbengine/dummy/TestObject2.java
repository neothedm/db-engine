package dm.dbengine.dummy;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class TestObject2 extends TestObject {

    private long id;
    private String description;
    private String comment;
    @OneToMany
    private List<TestObject> objects;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<TestObject> getObjects() {
        return objects;
    }

    public void setObjects(List<TestObject> objects) {
        this.objects = objects;
    }
}
