package dm.dbengine;

import dm.dbengine.connection.impl.DefaultConnectionFactory;
import dm.dbengine.connection.pool.impl.DefaultConnectionPoolFactory;
import dm.dbengine.dialects.CreateTableStatement;
import dm.dbengine.dialects.standard.StandardDialect;
import dm.dbengine.dummy.TestObject2;
import dm.dbengine.models.impl.DefaultModelBuilder;
import dm.dbengine.models.mapper.impl.DefaultMapper;
import dm.dbengine.transactions.Transaction;
import dm.dbengine.util.DatabaseConfig;
import dm.dbengine.util.Executor;
import junit.framework.TestCase;

import java.sql.SQLException;
import java.util.List;

public class TestCreateTable extends TestCase {

    public void setUp() {
        DatabaseConfig config = new DatabaseConfig();

        config.setJdbcUrl("jdbc:mysql://localhost:3306/cato");
        config.setUsername("root");
        config.setPassword("1016760");
        config.setMaxConnections(3);
        config.setConnectionMaxLife(20000);
        config.setDialect(StandardDialect.class);

        DbEngine.setConnectionPoolFactory(new DefaultConnectionPoolFactory());
        DbEngine.setConfig(config);
        DbEngine.setConnectionFactory(new DefaultConnectionFactory());
        DbEngine.setModelBuilder(new DefaultModelBuilder());
        DbEngine.setMapper(new DefaultMapper());
        DbEngine.init();
    }

    public void testCreateTable() {
//        Transaction.execute(() -> {
//            Statement statement = QueryBuilder.buildCreateTable(TestObject2.class);
//            System.out.println(statement.toString());
//            Executor.execute(statement);
//            return null;
//        });

        Transaction.execute(() -> {
            List<CreateTableStatement> statements = DbEngine.getDialect().getQueryBuilder().buildCreateTable(TestObject2.class);
            statements.forEach(s -> {
                System.out.println(s.toString());
//                try {
//                    Executor.execute(s);
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
            });
            return null;
        });
    }
}
