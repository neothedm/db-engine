package dm.dbengine;

import dm.dbengine.connection.impl.DefaultConnectionFactory;
import dm.dbengine.connection.pool.impl.DefaultConnectionPoolFactory;
import dm.dbengine.transactions.Transaction;
import dm.dbengine.util.DatabaseConfig;
import junit.framework.TestCase;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;
import java.util.concurrent.Callable;

public class TestConnection extends TestCase {

    public void setUp() {
        DatabaseConfig config = new DatabaseConfig();

        config.setJdbcUrl("jdbc:mysql://localhost:3306/cato");
        config.setUsername("root");
        config.setPassword("1016760");
        config.setMaxConnections(3);
        config.setConnectionMaxLife(20000);

        DbEngine.setConnectionPoolFactory(new DefaultConnectionPoolFactory());
        DbEngine.setConfig(config);
        DbEngine.setConnectionFactory(new DefaultConnectionFactory());
        DbEngine.init();
    }

    public void testConnection() throws SQLException {
        Connection connection = DbEngine.getConnectionPool().getConnection();
        Statement statement = connection.createStatement();
        statement.execute("SELECT * FROM Location");
        ResultSet resultSet = statement.getResultSet();
        assertTrue(resultSet.next());
        DbEngine.getConnectionPool().release(connection);
    }

    public void testMaxConnections() throws SQLException {
        Connection connection1 = DbEngine.getConnectionPool().getConnection();
        assertNotNull(connection1);
        Connection connection2 = DbEngine.getConnectionPool().getConnection();
        assertNotNull(connection2);
        Connection connection3 = DbEngine.getConnectionPool().getConnection();
        assertNotNull(connection3);
        Connection connection4 = DbEngine.getConnectionPool().getConnection();
        assertNull(connection4);
        DbEngine.getConnectionPool().release(connection1);
        DbEngine.getConnectionPool().release(connection2);
        DbEngine.getConnectionPool().release(connection3);
    }

    public void testTransaction() {
        Transaction.execute(() -> {
            String uuid = UUID.randomUUID().toString();
            String url = UUID.randomUUID().toString();
            Statement statement = Transaction.getConnection().createStatement();
            statement.execute(String.format("Insert into Location (uuid, url, quality, status) values ('%s', '%s', 1231, 0)", uuid, url));

            statement = Transaction.getConnection().createStatement();
            statement.execute(String.format("Select * from Location where uuid = '%s'", uuid));
            ResultSet resultSet = statement.getResultSet();
            assertTrue(resultSet.next());
            assertTrue(url.equals(resultSet.getString("url")));
            return null;
        });
    }

    public void testTransactionRollback() {
        String uuid = UUID.randomUUID().toString();
        String url = UUID.randomUUID().toString();
        try {
            Transaction.execute(() -> {
                Statement statement = Transaction.getConnection().createStatement();
                statement.execute(String.format("Insert into Location (uuid, url, quality, status) values ('%s', '%s', 1231, 0)", uuid, url));

                statement = Transaction.getConnection().createStatement();
                statement.execute(String.format("Select * from Location where uuid = '%s'", uuid));
                ResultSet resultSet = statement.getResultSet();
                assertTrue(resultSet.next());
                assertTrue(url.equals(resultSet.getString("url")));
                throw new IllegalStateException();
            });
        } catch (Throwable ignored) {}

        Transaction.execute(() -> {
            Statement statement = Transaction.getConnection().createStatement();
            statement.execute(String.format("Select * from Location where uuid = '%s'", uuid));
            ResultSet resultSet = statement.getResultSet();
            assertFalse(resultSet.next());
            return false;
        });
    }

    public void testConnectionRelease() throws SQLException {
        Connection connection1 = DbEngine.getConnectionPool().getConnection();
        assertNotNull(connection1);
        Connection connection2 = DbEngine.getConnectionPool().getConnection();
        assertNotNull(connection2);
        Connection connection3 = DbEngine.getConnectionPool().getConnection();
        assertNotNull(connection3);
        assertEquals(DbEngine.getConnectionPool().getAvailableConnectionsCount(), 0);
        DbEngine.getConnectionPool().release(connection1);
        DbEngine.getConnectionPool().release(connection2);
        DbEngine.getConnectionPool().release(connection3);
        assertEquals(DbEngine.getConnectionPool().getAvailableConnectionsCount(), 3);
    }
}
