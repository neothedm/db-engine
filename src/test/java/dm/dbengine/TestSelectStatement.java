package dm.dbengine;

import dm.dbengine.connection.impl.DefaultConnectionFactory;
import dm.dbengine.connection.pool.impl.DefaultConnectionPoolFactory;
import dm.dbengine.dialects.DeleteStatement;
import dm.dbengine.dialects.SelectStatement;
import dm.dbengine.dialects.standard.StandardDialect;
import dm.dbengine.dummy.TestObject2;
import dm.dbengine.models.impl.DefaultModelBuilder;
import dm.dbengine.models.mapper.impl.DefaultMapper;
import dm.dbengine.transactions.Transaction;
import dm.dbengine.util.DatabaseConfig;
import dm.dbengine.util.Executor;
import dm.dbengine.util.resultset.Processor;
import junit.framework.TestCase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class TestSelectStatement extends TestCase {

    public void setUp() {
        DatabaseConfig config = new DatabaseConfig();

        config.setJdbcUrl("jdbc:mysql://localhost:3306/cato");
        config.setUsername("root");
        config.setPassword("1016760");
        config.setMaxConnections(3);
        config.setConnectionMaxLife(20000);
        config.setDialect(StandardDialect.class);

        DbEngine.setConnectionPoolFactory(new DefaultConnectionPoolFactory());
        DbEngine.setConfig(config);
        DbEngine.setConnectionFactory(new DefaultConnectionFactory());
        DbEngine.setModelBuilder(new DefaultModelBuilder());
        DbEngine.setMapper(new DefaultMapper());
        DbEngine.init();
    }

    public void testSelectStatement() {
        Transaction.execute(() -> {
            String uuid = UUID.randomUUID().toString();
            String url = UUID.randomUUID().toString();
            Statement statement = Transaction.getConnection().createStatement();
            statement.execute(String.format("Insert into Location (uuid, url, status, quality) values ('%s', '%s', 0, 200)", uuid, url));

            SelectStatement selectStatement = DbEngine.getDialect().select();
            selectStatement.select("uuid", "url").from("Location").where("uuid = '" + uuid + "'");
            Executor.execute(selectStatement, (Processor<Object>) resultSet -> {
                assertTrue(resultSet.next());
                assertTrue(url.equals(resultSet.getString("url")));
                return null;
            });
            return null;
        });
    }

    public void testSelect() throws SQLException {
        SelectStatement selectStatement = DbEngine.getDialect().getQueryBuilder().buildSelect(TestObject2.class);
        List<TestObject2> object2List = Executor.execute(selectStatement, TestObject2.class);
        object2List.forEach(System.out::println);
        System.out.println(selectStatement.toString());
    }

    public void testDelete() throws SQLException {
        TestObject2 testObject = new TestObject2();
        testObject.setName("testName");
        testObject.setCode("testCod1e");
        testObject.setComment("sdasd");
        testObject.setDescription("asdasd");
        testObject.setTimeObject(new Date());
        testObject.setState(true);
        List<DeleteStatement> statements = DbEngine.getDialect().getQueryBuilder().buildDelete(testObject);
        statements.forEach(System.out::println);
    }
}
