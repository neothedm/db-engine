package dm.dbengine;

import dm.dbengine.connection.impl.DefaultConnectionFactory;
import dm.dbengine.connection.pool.impl.DefaultConnectionPoolFactory;
import dm.dbengine.dialects.standard.StandardDialect;
import dm.dbengine.models.ModelBuilder;
import dm.dbengine.models.impl.DbModel;
import dm.dbengine.models.impl.DefaultModelBuilder;
import dm.dbengine.util.DatabaseConfig;
import junit.framework.TestCase;

public class TestModelBuilder extends TestCase {

    public void setUp() {
        DatabaseConfig config = new DatabaseConfig();

        config.setJdbcUrl("jdbc:mysql://localhost:3306/cato");
        config.setUsername("root");
        config.setPassword("1016760");
        config.setMaxConnections(3);
        config.setConnectionMaxLife(20000);
        config.setDialect(StandardDialect.class);

        DbEngine.setConnectionPoolFactory(new DefaultConnectionPoolFactory());
        DbEngine.setConfig(config);
        DbEngine.setConnectionFactory(new DefaultConnectionFactory());
        DbEngine.setModelBuilder(new DefaultModelBuilder());
        DbEngine.init();
    }

    public void testModelBuilder() {
        ModelBuilder builder = new DefaultModelBuilder();
        DbModel model = builder.getModel(MockObject.class);
    }

    public void testSave() {
        MockObject mockObject = new MockObject();
        mockObject.id = "test123";
        mockObject.date = System.currentTimeMillis();
//        DbEngine.save(mockObject);
    }

    public class MockObject {

        private String id;
        private long date;
    }
}
