package dm.dbengine;

import dm.dbengine.connection.impl.DefaultConnectionFactory;
import dm.dbengine.connection.pool.impl.DefaultConnectionPoolFactory;
import dm.dbengine.dao.impl.DefaultDao;
import dm.dbengine.dialects.standard.StandardDialect;
import dm.dbengine.dummy.TestObject;
import dm.dbengine.dummy.TestObject2;
import dm.dbengine.models.impl.DefaultModelBuilder;
import dm.dbengine.models.mapper.impl.DefaultMapper;
import dm.dbengine.transactions.Transaction;
import dm.dbengine.util.DatabaseConfig;
import dm.dbengine.util.Executor;
import junit.framework.TestCase;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class TestDao extends TestCase {

    public void setUp() {
        DatabaseConfig config = new DatabaseConfig();

        config.setJdbcUrl("jdbc:mysql://localhost:3306/cato");
        config.setUsername("root");
        config.setPassword("1016760");
        config.setMaxConnections(3);
        config.setConnectionMaxLife(20000);
        config.setDialect(StandardDialect.class);

        DbEngine.setConnectionPoolFactory(new DefaultConnectionPoolFactory());
        DbEngine.setConfig(config);
        DbEngine.setConnectionFactory(new DefaultConnectionFactory());
        DbEngine.setModelBuilder(new DefaultModelBuilder());
        DbEngine.setMapper(new DefaultMapper());
        DbEngine.init();
    }

    public void testDao() {
        TestObject testObject = new TestObject();
        testObject.setName("testName");
        testObject.setCode("testCode");
        testObject.setTimeObject(new Date());
        testObject.setState(true);
        DefaultDao<TestObject, Long> dao = new DefaultDao<>(TestObject.class);
        dao.save(testObject);

        List<TestObject> testObjects = dao.getBy("name", "testName");
        assertFalse(testObjects.isEmpty());
    }

    public void testInsert() {
        TestObject2 testObject = new TestObject2();
        testObject.setName("testName");
        testObject.setCode("testCod1e");
        testObject.setComment("sdasd");
        testObject.setDescription("asdasd");
        testObject.setTimeObject(new Date());
        testObject.setState(true);
        Transaction.execute(() -> {
            DbEngine.getDialect().getQueryBuilder().buildInsert(testObject).forEach(s -> {
                System.out.println(s.toString());
                try {
                    Executor.execute(s);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
            return null;
        });
    }
}
